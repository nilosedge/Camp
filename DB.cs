using System;
using System.Collections;
using System.IO;

namespace Project
{
	public class DB
	{
		private string filename;
		private string tempfilename;

		public DB(string s)
		{
			filename = s;
			tempfilename = "tempfile224892";
		}
		public bool isInActivity(Person person, Activity act)
		{
			try 
			{
				Entry ent;
				StreamReader sr = new StreamReader(
					(System.IO.Stream)File.OpenRead(this.filename),
					System.Text.Encoding.ASCII);
				string line;
				string[] arr;

				sr.BaseStream.Seek(0, SeekOrigin.Begin);
				while ( (line = sr.ReadLine()) != null ) 
				{
					ent = new Entry(line);
					if ( ent.person.Text == person.Text && ent.activity.Text == act.Text ) 
					{
						sr.Close();
						return true;
					}
				}

				sr.Close();
				return false;
			}
			catch ( FileNotFoundException ) 
			{
				return false;
			}
		}
//////////////////////////////////
///StreamReader versions
//////////////////////////////////
		public Activity getActivity(Person person, Period period) 
		{
			try
			{
				StreamReader sr = new StreamReader(
					(System.IO.Stream)File.OpenRead(this.filename),
					System.Text.Encoding.ASCII);
				Entry ent;
				string line;

				sr.BaseStream.Seek(0, SeekOrigin.Begin);
				while ( (line = sr.ReadLine()) != null ) 
				{
					ent = new Entry(line);
					if ( ent.person.Text == person.Text &&
						ent.period.Text == period.Text )
					{
						sr.Close();
						return ent.activity;
					}
				}

				sr.Close();
				return null;
			}
			catch ( FileNotFoundException ) 
			{
				return null;
			}
		}
		public Period getPeriod(Person person, Activity act) 
		{
			try 
			{
				Entry ent;
				string line;
				StreamReader sr = new StreamReader(
					(System.IO.Stream)File.OpenRead(this.filename),
					System.Text.Encoding.ASCII);

				sr.BaseStream.Seek(0, SeekOrigin.Begin);
				while ( (line = sr.ReadLine()) != null ) 
				{
					ent = new Entry(line);
					if ( ent.person.Text == person.Text &&
						ent.activity.Text == act.Text )
					{
						sr.Close();
						return ent.period;
					}
				}

				sr.Close();
				return null;
			}
			catch ( FileNotFoundException ) 
			{
				return null;
			}
		}
		public int getCount(Activity a, Period p) 
		{
			try 
			{
				Entry ent;
				string line;
				int count = 0;
				StreamReader sr = new StreamReader(
					(System.IO.Stream)File.OpenRead(this.filename),
					System.Text.Encoding.ASCII);

				sr.BaseStream.Seek(0, SeekOrigin.Begin);

				while ( (line = sr.ReadLine()) != null ) 
				{
					ent = new Entry(line);
					if ( ent.activity.Text == a.Text &&
						ent.period.Text == p.Text )
					{
						count++;
					}
				}

				sr.Close();
				return count;
			}
			catch ( FileNotFoundException ) 
			{
				return 0;
			}
		}
		public Person[] getActivityRoster(Activity a, Period p)
		{
			try 
			{
				ArrayList temp = new ArrayList();
				Entry ent;
				Person[] list;
				string line;
				StreamReader sr = new StreamReader(
					(System.IO.Stream)File.OpenRead(this.filename),
					System.Text.Encoding.ASCII);

				sr.BaseStream.Seek(0, SeekOrigin.Begin);

				while ( (line = sr.ReadLine()) != null ) 
				{
					ent = new Entry(line);
					if ( ent.activity.Text == a.Text &&
						ent.period.Text == p.Text )
					{
						temp.Add(ent.person);
					}
				}

				sr.Close();
				list = new Person[temp.Count];
				temp.CopyTo(list);
				return list;
			}
			catch ( FileNotFoundException ) 
			{
				return new Person[0];
			}
		}
		public int addEntry(Entry e)
		{
			StreamWriter sw = new StreamWriter(
				(System.IO.Stream)File.OpenWrite(this.filename),
				System.Text.Encoding.ASCII);

			sw.BaseStream.Seek(0, SeekOrigin.End);
			sw.WriteLine(e.Text);
			sw.Close();
			return 1;
		}
		public int removeEntry(Entry e)
		{
			try 
			{
				string line;
				int found = 0;
				StreamReader sr = new StreamReader(
					(System.IO.Stream)File.OpenRead(this.filename),
					System.Text.Encoding.ASCII);
				StreamWriter sw = new StreamWriter(
					(System.IO.Stream)File.Create(this.tempfilename),
					System.Text.Encoding.ASCII);

				sr.BaseStream.Seek(0, SeekOrigin.Begin);
				while ( (line = sr.ReadLine()) != null ) 
				{
					if ( line != e.Text ) 
					{
						sw.WriteLine(line);
					}
					else
						found = 1;
				}

				sr.Close();
				sw.Close();
				File.Delete(this.filename);
				File.Move(this.tempfilename, this.filename);

				return found;
			}
			catch ( FileNotFoundException ) 
			{
				return 0;
			}
		}
	}
}
