using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;

namespace Project
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class ActivityReg : System.Windows.Forms.Form
	{
		private string[] OptionList;
		private string ActivityDataFileName = "act.txt";
		private string ActivitySignupFileName = "db.txt";

		private bool CanSignup = true;

		private System.Windows.Forms.TextBox nameBox;
		private System.Windows.Forms.Button buttonAdd;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;

		private System.Windows.Forms.ComboBox[] boxarray;

		private System.Windows.Forms.ComboBox tuesdayBox1;
		private System.Windows.Forms.ComboBox tuesdayBox2;
		private System.Windows.Forms.ComboBox tuesdayBox3;
		private System.Windows.Forms.ComboBox wednesdayBox3;
		private System.Windows.Forms.ComboBox wednesdayBox2;
		private System.Windows.Forms.ComboBox wednesdayBox1;
		private System.Windows.Forms.ComboBox thursdayBox3;
		private System.Windows.Forms.ComboBox thursdayBox2;
		private System.Windows.Forms.ComboBox thursdayBox1;
		private System.Windows.Forms.ComboBox fridayBox2;
		private System.Windows.Forms.ComboBox fridayBox1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ListBox groupListBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ActivityReg()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			ActivityData actdata;
			ArrayList al = new ArrayList();
			StreamReader sr = new StreamReader(
				(System.IO.Stream)File.OpenRead(this.ActivityDataFileName),
				System.Text.Encoding.ASCII);
			string str;

			sr.BaseStream.Seek(0, SeekOrigin.Begin);
			al.Add("--none--");
			while ( (str = sr.ReadLine()) != null ) 
			{
				actdata = new ActivityData(str);
				al.Add(actdata.Name);
			}
			sr.Close();
			al.Add("--multiple--");
			this.OptionList = new String[al.Count];
			al.CopyTo(this.OptionList);

			this.tuesdayBox1.Items.AddRange(OptionList);
			this.tuesdayBox2.Items.AddRange(OptionList);
			this.tuesdayBox3.Items.AddRange(OptionList);
			this.wednesdayBox1.Items.AddRange(OptionList);
			this.wednesdayBox2.Items.AddRange(OptionList);
			this.wednesdayBox3.Items.AddRange(OptionList);
			this.thursdayBox1.Items.AddRange(OptionList);
			this.thursdayBox2.Items.AddRange(OptionList);
			this.thursdayBox3.Items.AddRange(OptionList);
			this.fridayBox1.Items.AddRange(OptionList);
			this.fridayBox2.Items.AddRange(OptionList);

			boxarray = new System.Windows.Forms.ComboBox[11];
			boxarray[0] = tuesdayBox1;
			boxarray[1] = tuesdayBox2;
			boxarray[2] = tuesdayBox3;
			boxarray[3] = wednesdayBox1;
			boxarray[4] = wednesdayBox2;
			boxarray[5] = wednesdayBox3;
			boxarray[6] = thursdayBox1;
			boxarray[7] = thursdayBox2;
			boxarray[8] = thursdayBox3;
			boxarray[9] = fridayBox1;
			boxarray[10] = fridayBox2;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Updates all of the dropdown boxes when the group selection changes
		/// This still needs to be done, but we need the back-end to do it.
		/// </summary>
		private void UpdateDisplay() 
		{
			System.Windows.Forms.ComboBox curBox;
			Period period;
			Person person;
			Activity[] actlist;
			DB db = new DB(this.ActivitySignupFileName);
			int d, p, i;
			int selcount;
			bool different;

			for ( d = 1; d <= 4; d++ )
			{
				for ( p = 1; p <= 3; p++ ) 
				{
					if ( d == 4 && p == 3 ) continue;
					curBox = boxarray[(d-1)*3+p-1];
					period = new Period(d, p);
					actlist = new Activity[groupListBox.Items.Count];
					selcount = 0;
					foreach ( string pname in groupListBox.SelectedItems ) 
					{
						person = new Person(pname);
						actlist[selcount] = db.getActivity(person, period);
						selcount++;
					}

					if ( selcount == 1 )
					{
						curBox.Enabled = true;
						CanSignup = false;
						curBox.SelectedIndex = -1;
						CanSignup = true;
						if ( actlist[0] == null ) continue;
						for ( i = 0; i < OptionList.Length; i++ ) 
						{
							if ( OptionList[i] == actlist[0].Text ) 
							{
								CanSignup = false;
								curBox.SelectedIndex = i;
								CanSignup = true;
							}
						}
						continue;
					}

					different = false;
					for ( i = 0; i < selcount-1; i++ )
					{
						if ( actlist[i] == null && actlist[i+1] != null )
						{
							different = true;
							break;
						}
						if ( actlist[i] != null && actlist[i+1] == null ) 
						{
							different = true;
							break;
						}
						if ( actlist[i] == null && actlist[i+1] == null )
							continue;
						if ( actlist[i].Text != actlist[i+1].Text )
							different = true;
					}

					if ( !different )
					{
						curBox.Enabled = true;
						CanSignup = false;
						curBox.SelectedIndex = -1;
						CanSignup = true;
						if ( actlist[0] != null ) 
						{
							for ( i = 0; i < OptionList.Length; i++ ) 
							{
								if ( OptionList[i] == actlist[0].Text ) 
								{
									CanSignup = false;
									curBox.SelectedIndex = i;
									CanSignup = true;
								}
							}
						}
					}
					else
					{
						CanSignup = false;
						curBox.SelectedIndex = -1;
						CanSignup = true;
						curBox.Enabled = false;
					}
				}
			}
		}

		private int GetActivityMax(Activity act)
		{
			ActivityData actdata;
			StreamReader sr = new StreamReader(
				(System.IO.Stream)File.OpenRead(this.ActivityDataFileName),
				System.Text.Encoding.ASCII);
			string str;
			int max = 0;

			sr.BaseStream.Seek(0, SeekOrigin.Begin);
			while ( (str = sr.ReadLine()) != null ) 
			{
				actdata = new ActivityData(str);
				if ( actdata.Name == act.Text )
					max = actdata.Max;
			}
			sr.Close();
			return max;
		}
		private void UnSignup(Period period)
		{
			DB db = new DB(this.ActivitySignupFileName);
			Entry ent;
			Activity act;
			Person person;

			foreach ( string name in groupListBox.SelectedItems )
			{
				person = new Person(name);
				act = db.getActivity(person, period);

				ent = new Entry(person, act, period);
				db.removeEntry(ent);
			}
		}
		private int Signup(Activity act, Period period)
		{
			Activity act2;
			DB db = new DB(this.ActivitySignupFileName);
			Person person;
			Entry ent;
			int count;

			foreach ( string name in groupListBox.SelectedItems )
			{
				person = new Person(name);

				act2 = db.getActivity(person, period);
				if ( act2 != null ) 
				{
					//Don't think this if statement is needed
					if ( act2.Text != act.Text )
					{
						ent = new Entry(person, act2, period);
						db.removeEntry(ent);
					}
				}

				//Check for multiple signups to one activity
				if ( db.isInActivity(person, act) )
				{
					MessageBox.Show("Already signed up for "+act.Text);
					return 0;
				}
				//Check for room in the class
				count = db.getCount(act, period);
				if ( count < GetActivityMax(act) )
				{
					ent = new Entry(person, act, period);
					db.addEntry(ent);
				}
				else
				{
					MessageBox.Show(act.Text+" is full.");
					return 0;
				}
			}
			return 1;
		}

		private void DropDownClickHandler(object sender, System.EventArgs e)
		{
			if ( groupListBox.Items.Count == 0 ) 
			{
				MessageBox.Show("You must add people to the group box.\n"+
				                "To add a person, type their name in the "+
				                "Name box a and hit enter or click Add.");
			}
			else if ( groupListBox.SelectedIndex == -1 )
			{
				MessageBox.Show("You must select one or more people first.\n"+
				                "Click on a name in the group box to select that person.");
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.nameBox = new System.Windows.Forms.TextBox();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tuesdayBox1 = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.tuesdayBox2 = new System.Windows.Forms.ComboBox();
			this.tuesdayBox3 = new System.Windows.Forms.ComboBox();
			this.wednesdayBox3 = new System.Windows.Forms.ComboBox();
			this.wednesdayBox2 = new System.Windows.Forms.ComboBox();
			this.wednesdayBox1 = new System.Windows.Forms.ComboBox();
			this.thursdayBox3 = new System.Windows.Forms.ComboBox();
			this.thursdayBox2 = new System.Windows.Forms.ComboBox();
			this.thursdayBox1 = new System.Windows.Forms.ComboBox();
			this.fridayBox2 = new System.Windows.Forms.ComboBox();
			this.fridayBox1 = new System.Windows.Forms.ComboBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.groupListBox = new System.Windows.Forms.ListBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// nameBox
			// 
			this.nameBox.AcceptsReturn = true;
			this.nameBox.Location = new System.Drawing.Point(216, 32);
			this.nameBox.Name = "nameBox";
			this.nameBox.Size = new System.Drawing.Size(160, 20);
			this.nameBox.TabIndex = 1;
			this.nameBox.Text = "";
			// 
			// buttonAdd
			// 
			this.buttonAdd.Location = new System.Drawing.Point(224, 64);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.Size = new System.Drawing.Size(64, 24);
			this.buttonAdd.TabIndex = 2;
			this.buttonAdd.Text = "Add";
			this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(304, 64);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(64, 24);
			this.buttonClear.TabIndex = 4;
			this.buttonClear.Text = "Clear All";
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(216, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 16);
			this.label1.TabIndex = 5;
			this.label1.Text = "Name";
			// 
			// tuesdayBox1
			// 
			this.tuesdayBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.tuesdayBox1.Location = new System.Drawing.Point(16, 8);
			this.tuesdayBox1.MaxDropDownItems = 10;
			this.tuesdayBox1.Name = "tuesdayBox1";
			this.tuesdayBox1.Size = new System.Drawing.Size(96, 21);
			this.tuesdayBox1.TabIndex = 7;
			this.tuesdayBox1.Click += new System.EventHandler(this.DropDownClickHandler);
			this.tuesdayBox1.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(256, 104);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 16);
			this.label2.TabIndex = 19;
			this.label2.Text = "Tuesday";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(384, 104);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(80, 16);
			this.label3.TabIndex = 20;
			this.label3.Text = "Wednesday";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(512, 104);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 16);
			this.label4.TabIndex = 21;
			this.label4.Text = "Thursday";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(648, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 16);
			this.label5.TabIndex = 22;
			this.label5.Text = "Friday";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tuesdayBox2
			// 
			this.tuesdayBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.tuesdayBox2.Location = new System.Drawing.Point(16, 40);
			this.tuesdayBox2.MaxDropDownItems = 10;
			this.tuesdayBox2.Name = "tuesdayBox2";
			this.tuesdayBox2.Size = new System.Drawing.Size(96, 21);
			this.tuesdayBox2.TabIndex = 23;
			this.tuesdayBox2.Click += new System.EventHandler(this.DropDownClickHandler);
			this.tuesdayBox2.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// tuesdayBox3
			// 
			this.tuesdayBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.tuesdayBox3.Location = new System.Drawing.Point(16, 72);
			this.tuesdayBox3.MaxDropDownItems = 10;
			this.tuesdayBox3.Name = "tuesdayBox3";
			this.tuesdayBox3.Size = new System.Drawing.Size(96, 21);
			this.tuesdayBox3.TabIndex = 24;
			this.tuesdayBox3.Click += new System.EventHandler(this.DropDownClickHandler);
			this.tuesdayBox3.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// wednesdayBox3
			// 
			this.wednesdayBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.wednesdayBox3.Location = new System.Drawing.Point(144, 72);
			this.wednesdayBox3.MaxDropDownItems = 10;
			this.wednesdayBox3.Name = "wednesdayBox3";
			this.wednesdayBox3.Size = new System.Drawing.Size(96, 21);
			this.wednesdayBox3.TabIndex = 27;
			this.wednesdayBox3.Click += new System.EventHandler(this.DropDownClickHandler);
			this.wednesdayBox3.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// wednesdayBox2
			// 
			this.wednesdayBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.wednesdayBox2.Location = new System.Drawing.Point(144, 40);
			this.wednesdayBox2.MaxDropDownItems = 10;
			this.wednesdayBox2.Name = "wednesdayBox2";
			this.wednesdayBox2.Size = new System.Drawing.Size(96, 21);
			this.wednesdayBox2.TabIndex = 26;
			this.wednesdayBox2.Click += new System.EventHandler(this.DropDownClickHandler);
			this.wednesdayBox2.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// wednesdayBox1
			// 
			this.wednesdayBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.wednesdayBox1.Location = new System.Drawing.Point(144, 8);
			this.wednesdayBox1.MaxDropDownItems = 10;
			this.wednesdayBox1.Name = "wednesdayBox1";
			this.wednesdayBox1.Size = new System.Drawing.Size(96, 21);
			this.wednesdayBox1.TabIndex = 25;
			this.wednesdayBox1.Click += new System.EventHandler(this.DropDownClickHandler);
			this.wednesdayBox1.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// thursdayBox3
			// 
			this.thursdayBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.thursdayBox3.Location = new System.Drawing.Point(272, 72);
			this.thursdayBox3.MaxDropDownItems = 10;
			this.thursdayBox3.Name = "thursdayBox3";
			this.thursdayBox3.Size = new System.Drawing.Size(96, 21);
			this.thursdayBox3.TabIndex = 30;
			this.thursdayBox3.Click += new System.EventHandler(this.DropDownClickHandler);
			this.thursdayBox3.SelectedIndexChanged += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// thursdayBox2
			// 
			this.thursdayBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.thursdayBox2.Location = new System.Drawing.Point(272, 40);
			this.thursdayBox2.MaxDropDownItems = 10;
			this.thursdayBox2.Name = "thursdayBox2";
			this.thursdayBox2.Size = new System.Drawing.Size(96, 21);
			this.thursdayBox2.TabIndex = 29;
			this.thursdayBox2.Click += new System.EventHandler(this.DropDownClickHandler);
			this.thursdayBox2.SelectedIndexChanged += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// thursdayBox1
			// 
			this.thursdayBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.thursdayBox1.Location = new System.Drawing.Point(272, 8);
			this.thursdayBox1.MaxDropDownItems = 10;
			this.thursdayBox1.Name = "thursdayBox1";
			this.thursdayBox1.Size = new System.Drawing.Size(96, 21);
			this.thursdayBox1.TabIndex = 28;
			this.thursdayBox1.Click += new System.EventHandler(this.DropDownClickHandler);
			this.thursdayBox1.SelectionChangeCommitted += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// fridayBox2
			// 
			this.fridayBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.fridayBox2.Location = new System.Drawing.Point(408, 40);
			this.fridayBox2.MaxDropDownItems = 10;
			this.fridayBox2.Name = "fridayBox2";
			this.fridayBox2.Size = new System.Drawing.Size(96, 21);
			this.fridayBox2.TabIndex = 32;
			this.fridayBox2.Click += new System.EventHandler(this.DropDownClickHandler);
			this.fridayBox2.SelectedIndexChanged += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// fridayBox1
			// 
			this.fridayBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.fridayBox1.Location = new System.Drawing.Point(408, 8);
			this.fridayBox1.MaxDropDownItems = 10;
			this.fridayBox1.Name = "fridayBox1";
			this.fridayBox1.Size = new System.Drawing.Size(96, 21);
			this.fridayBox1.TabIndex = 31;
			this.fridayBox1.Click += new System.EventHandler(this.DropDownClickHandler);
			this.fridayBox1.SelectedIndexChanged += new System.EventHandler(this.SelectionChangeCommitted);
			// 
			// panel1
			// 
			this.panel1.Controls.AddRange(new System.Windows.Forms.Control[] {
																				 this.thursdayBox3,
																				 this.wednesdayBox1,
																				 this.wednesdayBox2,
																				 this.tuesdayBox1,
																				 this.fridayBox1,
																				 this.wednesdayBox3,
																				 this.fridayBox2,
																				 this.tuesdayBox3,
																				 this.tuesdayBox2,
																				 this.thursdayBox1,
																				 this.thursdayBox2});
			this.panel1.Location = new System.Drawing.Point(232, 120);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(520, 112);
			this.panel1.TabIndex = 34;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(72, 16);
			this.label6.TabIndex = 33;
			this.label6.Text = "Group";
			// 
			// groupListBox
			// 
			this.groupListBox.Location = new System.Drawing.Point(16, 32);
			this.groupListBox.Name = "groupListBox";
			this.groupListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.groupListBox.Size = new System.Drawing.Size(152, 199);
			this.groupListBox.TabIndex = 6;
			this.groupListBox.SelectedIndexChanged += new System.EventHandler(this.groupListBox_SelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(192, 136);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(56, 16);
			this.label7.TabIndex = 35;
			this.label7.Text = "Period 1:";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(192, 168);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(56, 16);
			this.label8.TabIndex = 36;
			this.label8.Text = "Period 2:";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(192, 200);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(56, 16);
			this.label9.TabIndex = 37;
			this.label9.Text = "Period 3:";
			// 
			// ActivityReg
			// 
			this.AcceptButton = this.buttonAdd;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(776, 317);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label9,
																		  this.label8,
																		  this.label7,
																		  this.panel1,
																		  this.buttonAdd,
																		  this.buttonClear,
																		  this.nameBox,
																		  this.label1,
																		  this.groupListBox,
																		  this.label6,
																		  this.label4,
																		  this.label2,
																		  this.label5,
																		  this.label3});
			this.Name = "ActivityReg";
			this.Text = "ActivityReg";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ActivityReg());
		}

		private void buttonAdd_Click(object sender, System.EventArgs e)
		{
			groupListBox.Items.Add(nameBox.Text);
			nameBox.Clear();
		}

		private void buttonClear_Click(object sender, System.EventArgs e)
		{
			groupListBox.Items.Clear();
		}

		private void groupListBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			UpdateDisplay();
		}

		private void SelectionChangeCommitted(object sender, System.EventArgs e)
		{
			if ( !CanSignup ) return;

			int i;
			int d, p;

			System.Windows.Forms.ComboBox box = sender as System.Windows.Forms.ComboBox;

			if ( box == null )
			{
				MessageBox.Show("Object Casting Error\n");
				return;
			}


			i = Array.IndexOf(this.boxarray, box, 0, boxarray.Length);

			//Convert index to day and period
			p = i%3+1;
			d = i/3+1;
			
			if ( box.SelectedIndex == 0 )
			{
				CanSignup = false;
				box.SelectedIndex = -1;
				CanSignup = true;
			}
			if ( box.SelectedIndex == -1 )
			{
				UnSignup(new Period(d, p));
			} 
			else 
			{
				if ( Signup(new Activity(box.SelectedItem.ToString()),new Period(d, p)) == 0 )
				{
					CanSignup = false;
					box.SelectedIndex = -1;
					CanSignup = true;
				}
			}
		}
	}
}
