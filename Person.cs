using System;
using System.Text;

namespace Project
{
	public class Person
	{
		private string FirstName;
		private string LastName;

		public Person(string name)
		{
			int i;

			StringBuilder temp = new StringBuilder();

			for ( i = 0; i < name.Length; i++ ) 
			{
				if ( name[i] == ' ' ) 
				{
					i++;
					break;
				}
				temp.Append(name[i]);
			}
			FirstName = temp.ToString();

			temp = new StringBuilder();
			while ( i < name.Length ) 
			{
				temp.Append(name[i]);
				i++;
			}
			LastName = temp.ToString();
		}

		public Person(string first, string last)
		{
			FirstName = first;
			LastName = last;
		}

		public string Text
		{
			get
			{
				return FirstName+" "+LastName;
			}
		}
	}
}
