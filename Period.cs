using System;
using System.Data;

namespace Project
{
	public class Period
	{
		private int _day;
		private int _period;

		public Period(int d, int p)
		{
			_day = d;
			_period = p;
		}
		public Period(string d, string p)
		{
			_day = int.Parse(d);
			_period = int.Parse(p);
		}
		public int day
		{
			get
			{
				return _day;
			}

			set
			{
				_day = ((value >= 0 && value < 4) ? value : 0 );
			}
		}

		public int period
		{
			get
			{
				return _period;
			}

			set
			{
				_period = ( (value >= 1 && value <= 3) ? value : 1 );
			}
		}
		public string Text
		{
			get 
			{
				return _day.ToString()+","+_period.ToString();
			}
		}
	}
}
