using System;
using System.IO;
using System.Text;

namespace Project
{
	/// <summary>
	/// Summary description for DBReader.
	/// </summary>
	class DBWriter
	{
		string filename;
		FileStream fs;
		BinaryWriter bw;

		public DBWriter(string s)
		{
			filename = s;
			this.Open();
		}
		~DBWriter()
		{
			this.Close();
		}

		public void Open()
		{
			fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
			fs.Seek(0, SeekOrigin.Begin);
			bw = new BinaryWriter(fs);
		}

		public void Close()
		{
			bw.Close();
			fs.Close();
		}

		public void Flush()
		{
			bw.Flush();
		}

		public void WriteLine(string line)
		{
			bw.Seek(0, SeekOrigin.End);
			bw.Write(line);
			bw.Flush();
		} 
	}
}
