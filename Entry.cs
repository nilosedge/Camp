using System;

namespace Project
{
	/// <summary>
	/// Summary description for Entry.
	/// </summary>
	public class Entry
	{
		private Person _person;
		private Activity _activity;
		private Period _period;

		public Entry(string line)
		{
			string[] arr;
			
			arr = this.ParseLine(line);
			this._person = new Person(arr[0]);
			this._activity = new Activity(arr[1]);
			this._period = new Period(arr[2], arr[3]);
		}

		public Entry(Person pers, Activity act, Period per)
		{
			this._person = pers;
			this._activity = act;
			this._period = per;
		}

		private string[] ParseLine(string s) 
		{
			string[] strarr;
			int i, count = 1;

			for ( i = 0; i < s.Length; i++ ) 
			{
				if ( s[i] == ',' ) count++;
			}
			strarr = new String[count];

			count = 0;
			for ( i = 0; i < s.Length; i++ ) 
			{
				if ( s[i] == ',' ) 
				{
					count++;
				} 
				else 
				{
					strarr[count] += s[i];
				}
			}

			return strarr;
		}

		public string Text 
		{
			get
			{
				return _person.Text+","+_activity.Text+","+_period.Text;
			}
		}
		public Person person
		{
			get
			{
				return _person;
			}
		}
		public Activity activity
		{
			get
			{
				return _activity;
			}
		}
		public Period period
		{
			get
			{
				return _period;
			}
		}
	}
}
