using System;
using System.IO;
using System.Text;

namespace Project
{
	/// <summary>
	/// Summary description for DBReader.
	/// </summary>
	class DBReader
	{
		string filename;
		FileStream fs;
		BinaryReader br;

		public DBReader(string s)
		{
			filename = s;
			this.Open();
		}
		~DBReader()
		{
			this.Close();
		}

		public void Open()
		{
			fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			br = new BinaryReader(fs);
		}

		public void Close()
		{
			br.Close();
			fs.Close();
		}

		public string ReadLine()
		{
			StringBuilder temp = new StringBuilder(50);
			string str;
			char  c;

			try
			{
				while ( (c = br.ReadChar()) != '\n' ) 
				{
					//if ( b == '\n' ) break;
					temp.Append(c);
				}
			}
			catch ( EndOfStreamException ) 
			{
				return "";
			}

			str = temp.ToString();
			return str;
		} 
		public string[] ParseLine(string s) 
		{
			string[] strarr;
			int i, count = 1;

			for ( i = 0; i < s.Length; i++ ) 
			{
				if ( s[i] == ',' ) count++;
			}
			strarr = new String[count];

			count = 0;
			for ( i = 0; i < s.Length; i++ ) 
			{
				if ( s[i] == ',' ) 
				{
					count++;
				} 
				else 
				{
					strarr[count] += s[i];
				}
			}

			return strarr;
		}

	}
}
