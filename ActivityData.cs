using System;

namespace Project
{
	public class ActivityData
	{
		private string _name;
		private int _max;

		public ActivityData(string line)
		{
			string[] arr;
			
			arr = this.ParseLine(line);
			this._name = arr[0];
			this._max = Int32.Parse(arr[1]);
		}

		public ActivityData(string name, int max)
		{
			this._name = name;
			this._max = max;
		}

		private string[] ParseLine(string s) 
		{
			string[] strarr;
			int i, count = 1;

			for ( i = 0; i < s.Length; i++ ) 
			{
				if ( s[i] == ',' ) count++;
			}
			strarr = new String[count];

			count = 0;
			for ( i = 0; i < s.Length; i++ ) 
			{
				if ( s[i] == ',' ) 
				{
					count++;
				} 
				else 
				{
					strarr[count] += s[i];
				}
			}

			return strarr;
		}

		public string Text 
		{
			get
			{
				return _name+_max.ToString();
			}
		}
		public string Name
		{
			get
			{
				return _name;
			}
		}
		public int Max
		{
			get
			{
				return _max;
			}
		}
	}
}
