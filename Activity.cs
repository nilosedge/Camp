using System;

namespace Project
{
	/// <summary>
	/// Summary description for Activity.
	/// </summary>
	public class Activity
	{
		private string name;

		public Activity(string s)
		{
			name = s;
		}

		public string Text 
		{
			get
			{
				return name;
			}
		}


	}
}
